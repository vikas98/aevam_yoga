import React from 'react';
import { useEffect } from 'react';
import { Text, View, ImageBackground } from 'react-native';
const Splash=({navigation})=>{
  useEffect(()=>{
    setTimeout(function(){
            navigation.navigate('SignIn')
      }, 2000);
})
    return(
     <View>
    <ImageBackground source={require('../../assets/drawn-fantasy.jpg')} style={{height:'100%', width:'100%'}}>
      <View style={{alignItems:'center', marginTop:'30%'}}>
      <Text style={{color:'#99003d', fontSize:20, fontWeight:'bold'}}>AEVAM YOGA</Text>
      </View>
    </ImageBackground>
     </View>
    )
}
export default Splash;