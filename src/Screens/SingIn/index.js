import React from 'react';
import { Image, ImageBackground, Text, TouchableOpacity, View } from 'react-native';
const SignIn = ({navigation}) => {
  return (
    <View>
      <ImageBackground source={require('../../assets/drawn-fantasy.jpg')} style={{ height: '100%', width: '100%' }}>
        <View>
          <View style={{ flexDirection: 'column', alignItems: "center", marginTop: '10%' }}>
            <Text style={{ color: '#99003d', fontSize: 30, fontWeight: 'bold' }}>Aevam Yoga</Text>
            <Text style={{ color: '#99003d', fontSize: 20, fontWeight: 'bold', marginTop:'10%' }}>Welcome to the Family</Text>
              <Text style={{ color: '#99003d', fontSize: 20, fontWeight: 'bold', marginTop:'10%' }}>Help us keep your profile in sync </Text>
              <Text style={{ color: '#99003d', fontSize: 20, fontWeight: 'bold' }}>across all devices and Aevam Yoga</Text>           
          </View>
          <TouchableOpacity>
            <View style={{backgroundColor:'white', borderRadius:30, padding:15, marginTop:'10%', marginLeft:'10%',
             marginRight:'10%', alignItems:'center', flexDirection:'row'}}>
               <Image source={require('../../assets/google.png')} style={{height:30, width:30, marginLeft:'5%'}}></Image>
              <Text style={{ color: 'gray', fontSize: 20 , marginLeft:'5%'}}>Sign in with Google</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{backgroundColor:'#006080', borderRadius:30, padding:15, marginTop:'3%', marginLeft:'10%', 
            marginRight:'10%', alignItems:'center', flexDirection:'row'}}>
               <Image source={require('../../assets/facebook.png')} style={{height:25, width:25, marginLeft:'5%'}}></Image>
              <Text style={{ color: 'white', fontSize: 20, marginLeft:'5%' }}>Sign in with Facebook</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{backgroundColor:'white', borderRadius:30, padding:15, marginTop:'3%', marginLeft:'10%',
             marginRight:'10%',  alignItems:'center', flexDirection:'row'}}>
                <Image source={require('../../assets/apple.png')} style={{height:30, width:30, marginLeft:'5%'}}></Image>
              <Text style={{ color: 'black', fontSize: 20, marginLeft:'5%' }}>Sign in with Apple</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>navigation.navigate('Email')}>
            <View style={{ padding:12, marginTop:'5%', marginLeft:'20%', marginRight:'20%' , alignItems:'center'}}>
              <Text style={{ color: '#99003d', fontSize: 18, fontWeight:'bold' }}>Continue with e-mail</Text>
            </View>
          </TouchableOpacity>
          <View style={{ marginTop:'10%', marginLeft:'8%', marginRight:'8%'}}>
            <Text>
              <Text style={{ color: '#99003d', fontSize: 15, fontWeight:'bold' }}>By signing up you agree to </Text>
              <TouchableOpacity>
                <Text style={{ color: '#99003d', fontSize: 15, borderBottomWidth: 1, borderBottomColor:'#99003d', alignItems:'baseline' }}>Terms of Services</Text>
              </TouchableOpacity>
              <Text style={{ color: '#99003d', fontSize: 15, fontWeight:'bold' }}> and </Text>
              <TouchableOpacity>
                <Text style={{ color: '#99003d', fontSize: 15, borderBottomWidth: 1, borderBottomColor:'#99003d', alignItems:'baseline' }}>Privacy Policy</Text>
              </TouchableOpacity>
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}
export default SignIn;