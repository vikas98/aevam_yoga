import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, Alert, ImageBackground } from 'react-native';

const Email = ({ navigation }) => {
    return (
        <View>
            <ImageBackground source={require('../../assets/drawn-fantasy.jpg')} style={{ height: '100%', width: '100%' }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: '#99003d', fontSize: 30, fontWeight: 'bold', marginTop: '10%' }}>Aevam Yoga</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: '#99003d', fontSize: 30, fontWeight: 'bold', marginTop: '20%' }}>Enter your e-mail</Text>
                </View>
                <View style={{
                    flexDirection: 'row', alignItems: "center", marginLeft: '15%', marginRight: '15%',
                    borderBottomWidth: 1, borderBottomColor: '#99003d', marginTop: '20%'
                }}>
                    <Image source={require('../../assets/mail.png')} style={{ height: 23, width: 23 }}></Image>
                    <TextInput placeholder='E-mail' placeholderTextColor='#99003d' style={{ marginLeft: '3%' }} ></TextInput>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('SignUp')} >
                    <View style={{
                        backgroundColor: 'white', alignItems: "center", marginTop: '30%', borderRadius: 30, padding: 10,
                        marginLeft: '15%', marginRight: '15%'
                    }}>
                        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }}>Continue</Text>
                    </View>
                </TouchableOpacity>
            </ImageBackground>
        </View>
    )
}
export default Email;