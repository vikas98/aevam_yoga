import React, {useState} from 'react';
import { TextInput, View, Text, TouchableOpacity, ImageBackground, Image} from 'react-native';
import {SafeAreaView, Platform, PermissionsAndroid} from 'react-native';
import { launchCamera, launchImageLibrary} from 'react-native-image-picker';

const SignUp = ({ navigation }) => {
    const [filePath, setFilePath] = useState({});
    const [condition, setCondition] = useState(false);

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const captureImage = async (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      videoQuality: 'low',
      durationLimit: 30, //Video max duration in seconds
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        console.log('base64 -> ', response.base64);
        console.log('uri -> ', response.uri);
        console.log('width -> ', response.width);
        console.log('height -> ', response.height);
        console.log('fileSize -> ', response.fileSize);
        console.log('type -> ', response.type);
        console.log('fileName -> ', response.fileName);
        setFilePath(response);
      });
    }
  };

  const chooseFile = (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      console.log('base64 -> ', response.base64);
      console.log('uri -> ', response.uri);
      console.log('width -> ', response.width);
      console.log('height -> ', response.height);
      console.log('fileSize -> ', response.fileSize);
      console.log('type -> ', response.type);
      console.log('fileName -> ', response.fileName);
      setFilePath(response);
    });
  };
    return (
        <View>
            <ImageBackground source={require('../../assets/drawn-fantasy.jpg')} style={{ height: '100%', width: '100%'}}>
            <View style={{alignItems:'center'}}>
        <Text style={{ color: '#99003d', fontSize: 30, fontWeight: 'bold', marginTop:'10%' }}>Aevam Yoga</Text>
    </View>    

     <View style={{ alignSelf: 'center', marginTop: 20 }}>
          <TouchableOpacity onPress={() => setCondition(!condition)}>
          
            {filePath.uri==undefined?
           <Image source={require('../../assets/changeuser.png')} style={{height:100, width:100}}></Image>
            
          //  <Image source={require('../../assets/addevent.png')} style={{ height: 100, width: 100, borderRadius: 50 }}></Image>   
              :
              <Image source={{ uri:filePath.uri }} style={{ height: 100, width: 100, borderRadius: 50 }}></Image>
          }
          </TouchableOpacity>
        </View>
        {condition == true ?
          <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: "center" }}>
            <TouchableOpacity
              activeOpacity={0.5}
              // style={styles.buttonStyle}
              onPress={() => captureImage('photo')}>
                 <Image source={require('../../assets/camera.png')} style={{height:30, width:30}}></Image>
             
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => chooseFile('photo')}>
                 <Image source={require('../../assets/gallery.png')} style={{height:30, width:30}}></Image>
            </TouchableOpacity>
          </View> : null}
             

    {/* <View style={{alignItems:'center', marginTop:'10%'}}>
        <TouchableOpacity>
    <Image source={require('../../assets/changeuser.png')} style={{height:100, width:100}}></Image>
    </TouchableOpacity>
    </View> */}

                    <View style={{ borderBottomWidth: 1, borderBottomColor: '#99003d', marginTop: '10%', marginLeft: "13%", marginRight: '13%', flexDirection:'row', alignItems:'center' }}>
                    <Image source={require('../../assets/name.png')} style={{height:30, width:30}}></Image>
                        <TextInput placeholder='Your Name' placeholderTextColor='#99003d' style={{marginLeft:'3%'}}></TextInput>
                    </View>               
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#99003d', marginTop: '5%', marginLeft: "13%", marginRight: '13%', flexDirection:'row', alignItems:'center' }}>
                <Image source={require('../../assets/mail.png')} style={{height:23, width:23}}></Image>
                    <TextInput placeholder='Email' placeholderTextColor='#99003d' style={{marginLeft:'3%'}}></TextInput>
                </View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#99003d', marginTop: '5%', marginLeft: "13%", marginRight: '13%', flexDirection:'row', alignItems:'center' }}>
                <Image source={require('../../assets/eye2.png')} style={{height:30, width:30}}></Image>
                    <TextInput placeholder='Password' placeholderTextColor='#99003d' style={{marginLeft:'3%'}} secureTextEntry={true}></TextInput>
                </View>               
                <TouchableOpacity onPress={()=>navigation.navigate('BottomStack')}>
                    <View style={{
                        backgroundColor: 'white', alignItems: "center", marginTop: '10%', borderRadius: 30, padding: 10,
                        marginLeft: '15%', marginRight: '15%'
                    }}>
                        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }}>Sign Up</Text>
                    </View>
                </TouchableOpacity>
        
            </ImageBackground>
        </View>
    )
}
export default SignUp;