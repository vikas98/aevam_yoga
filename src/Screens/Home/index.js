import React, { useState } from 'react';
import { Text, View, ImageBackground, TouchableOpacity, Image, FlatList, Alert, ScrollView } from 'react-native';

const Home = () => {

    const [data, setData] = useState([
        { id: '1', title: 'Flexibility Essentials' },
        { id: '2', title: 'Strength Essentials' },
        { id: '3', title: 'Balance Essentials' },
        { id: '4', title: 'Supine Yoga Essentials' },
    ]);

    return (
        <View>
            <ImageBackground source={require('../../assets/drawn-fantasy.jpg')} style={{ height: '100%', width: '100%' }}>
                <TouchableOpacity>
                    <View style={{
                        backgroundColor: 'white', flexDirection: 'row', marginTop: '10%', marginLeft: '5%',
                        padding: 20, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, alignItems: 'center'
                    }}>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#99003d' }}>Your Daily Yoga</Text>
                            <Text>17 min</Text>
                        </View>
                        <View>
                            <Image source={require('../../assets/arrow.png')} style={{ height: 30, width: 15, marginLeft: '70%' }}></Image>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{ marginLeft: '5%', marginTop: "5%" }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#99003d' }}>YOGA ESSENTIALS</Text>
                </View>
                <View>

                    <FlatList
                    //  horizontal
                    //  pagingEnabled={true}
                    //  showsHorizontalScrollIndicator={false}
                    //  legacyImplementation={false}
                        data={data}
                        keyExtractor={item => item.id}
                        renderItem={({ item , id}) => (
                            <TouchableOpacity>
                                <View key={id} style={{  marginLeft: '5%', marginRight: '5%', marginTop:'5%' }}>
                                    <ImageBackground source={require('../../assets/bgimage.jpg')} style={{ height:100, width:100, borderRadius: 30 }}>
                                        <View style={{ backgroundColor: 'white', marginTop:'50%' }}>
                                            <Text style={{ fontSize: 20, color: '#99003d' }}>{item.title}</Text>
                                        </View>
                                    </ImageBackground>
                                </View> 
                            </TouchableOpacity>
                        )
                        }
                    />

                </View>
            </ImageBackground>
        </View>
    )
}
export default Home;