import React from 'react';
import {Text, Image} from 'react-native';
import Home from '../../Screens/Home/index';
import MyProfile from '../../Screens/MyProfile/index';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


const Tab1 = createBottomTabNavigator();
const BottomStack = () => {
    return (
        <Tab1.Navigator initialRouteName="Home"
            tabBarOptions={{
                activeTintColor: '#52BE80',
                //inactiveTintColor: '#99003d',
                
            // activeBackgroundColor: '#99003d',
                
            }}>
            <Tab1.Screen name="Home" component={Home} options={{
                 tabBarLabel: ({ focused }) => (
                    <Text style={{ fontSize: 15, color: '#99003d', fontWeight:'bold' }}>
                      Workouts
                    </Text>
                  ),
                labelStyle: '50',
                fontSize: 20,
                tabBarIcon: ({ color, size }) => (
                    <Image source={require('../../assets/workout2.png')} style={{height:30, width:30}}></Image>
                ),
            }} />
            <Tab1.Screen name="MyProfile" component={MyProfile} options={{
                tabBarLabel: ({ focused }) => (
                    <Text style={{ fontSize: 15, color: '#99003d', fontWeight:"bold" }}>
                      Profile
                    </Text>
                  ),
                labelStyle: 'bold',
                tabBarIcon: ({ color, size }) => (
                    <Image source={require('../../assets/userProfile.png')} style={{height:30, width:30}}></Image>
                ),
            }} />
        </Tab1.Navigator>
    )
}
export default BottomStack;