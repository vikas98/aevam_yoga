import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import Splash from '../../Screens/Splash';
import SignIn from '../../Screens/SingIn';
import Email from '../../Screens/Email';
import SignUp from '../../Screens/SignUp';
import BottomStack from '../BottomStack/index';
const MainStack =()=>{
    return(
    <Stack.Navigator>
        <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
        <Stack.Screen name="SignIn" component={SignIn} options={{headerShown:false}}/>
        <Stack.Screen name="Email" component={Email} options={{headerShown:false}}/>
        <Stack.Screen name="SignUp" component={SignUp} options={{headerShown:false}}/>
        <Stack.Screen name="BottomStack" component={BottomStack} options={{headerShown:false}}/>
    </Stack.Navigator>
    );
}
export default MainStack;